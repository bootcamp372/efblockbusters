﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFBlockbusters.Models
{
    public class Movie
    {
        public int MovieID { get; set; }
        public string Title { get; set; }

        [MaxLength(120)]
        public string Description { get; set; }
        public string Director { get; set; }

        public Category Category { get; set; }

        public int CategoryID { get; set; }

    }
}
