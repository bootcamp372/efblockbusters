﻿using EFBlockbusters.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Protocols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EFBlockbusters
{
    internal class BlockbusterMovieEntitiesContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Movie> Movies { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=Movies;Integrated Security=true;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            /*            modelBuilder.Entity<Category>()
                            .HasMany(category => category.Movies)
                            .WithOne(movie => movie.Category)
                            .HasForeignKey(movie => movie.CategoryID);*/

            modelBuilder.Entity<Movie>().HasData(
                new Movie
                {
                    MovieID = 1,
                    Title = "Face Off",
                    Description = "Face Off description",
                    /*                    Description = "A brilliant movie",
                    */
                    Director = "John Woo",
                    CategoryID = 1
                },
                new Movie
                {
                    MovieID = 2,
                    Title = "Shawshank Redemption",
                    Description = "Shawshank Redemption description",
                    /*                    Description = "Andy Dufresne (Tim Robbins) is sentenced to two consecutive life terms in prison for the murders of his wife and her lover and is sentenced to a tough prison. However, only Andy knows he didn't commit the crimes.",
                    */
                    Director = "Frank Darabont",
                    CategoryID = 1
                },
                new Movie
                {
                    MovieID = 3,
                    Title = "The Big Lebowski",
                    Description = "The Big Lebowski description",
                    /*                    Description = "Jeff `The Dude' Leboswki is mistaken for Jeffrey Lebowski, who is The Big Lebowski. Which explains why he's roughed up and has his precious rug peed on. In search of recompense, The Dude tracks down his namesake, who offers him a job.",
                    */
                    Director = "Ethan Coen and Joel Coen",
                    CategoryID = 2
                },
                new Movie
                {
                    MovieID = 4,
                    Title = "Pan's Labyrinth",
                    Description = "PL description",
                    /*                    Description = "Guillermo Del Toro's fairy tale for grown-ups, as pull-no-punches brutal as it is gorgeously, baroquely fantastical.",
                    */
                    Director = "Guillermo Del Toro",
                    CategoryID = 3
                },
                new Movie
                {
                    MovieID = 5,
                    Title = "Labyrinth",
                    Description = "Labyrinth description",
                    /* Description = "David Bowie as the Goblin King.",*/
                    Director = "Jim Henson",
                    CategoryID = 3
                }
            ); ;

            modelBuilder.Entity<Category>().HasData(
                    new Category
                    {
                        CategoryID = 1,
                        CategoryName = "Action"
                        
                    },
                    new Category
                    {
                        CategoryID = 2,
                        CategoryName = "Comedy"
                        
                    },
                    new Category
                    {
                        CategoryID = 3,
                        CategoryName = "Fantasy"

                    }
                 );
        }

    }
}
