﻿using EFBlockbusters.Models;
using Microsoft.Identity.Client;

namespace EFBlockbusters
{
    internal class Program
    {
        static void Main(string[] args)
        {
            DisplayCategories();
/*            GetProductByCategory();*/
        }

        static void DisplayCategories()
        {
            var context = new BlockbusterMovieEntitiesContext();
            var movieCategoriesQuery = from c in context.Categories
                                       orderby c.CategoryName
                                       select c;

            Console.WriteLine("Movie Categories are:");
            foreach (Category c in movieCategoriesQuery)
            {
                Console.WriteLine(
                $"{c.CategoryID} - {c.CategoryName}");
            }
            Console.WriteLine("What type of movie would you like to see?");
            string catg = Console.ReadLine();
            GetMoviesByCatg(catg); 

        }

        static void GetMoviesByCatg(string catg)
        {

            var context = new BlockbusterMovieEntitiesContext();
            var movieByCatgQuery =
                from m in context.Movies
                where m.Category.CategoryName == catg
                orderby m.Category.CategoryName
                select new
                {
                    m.MovieID,
                    m.Title,
                    m.Description,
                    m.Director,
                    Category = m.Category.CategoryName
                };

            foreach (var movie in movieByCatgQuery)
            {
                Console.WriteLine("-----------------");
                Console.WriteLine(
                    $"#{movie.MovieID} {movie.Title}");
                Console.WriteLine($"Category: {movie.Category}");
                Console.WriteLine($"Director: {movie.Director}");
                Console.WriteLine(
                    $"Description: {movie.Description}");
                Console.WriteLine("-----------------");
            }

        }
    }
}